#include "objdetect/objdetect.hpp"
#include "video/video.hpp"
#include "highgui/highgui.hpp"
#include "imgproc/imgproc.hpp"
#include "rsdgMission.h"

#include <cmath>

#include <iostream>
#include <stdio.h>
#include <ctime>

using namespace std;
using namespace cv;

rsdgMission* faceMission;
rsdgPara* pyramidPara;
rsdgPara* selectPara;
rsdgPara* eyesPara;
bool RSDG = false;
bool UPDATE = false;
bool TRAINING = false;
bool offline = false;
int totSec;
int totUnit;
string infile = "rsdgFace.xml";
string outfile = "output.lp";
int UNIT_PER_CHECK = 2;
int pyramid = 20;
int selectivity = 2;
int eyes = 2;

void setupMission(); // setup the rapid mission

void display(Mat image, std::vector<Rect> faces) {

    for (size_t i = 0; i < faces.size(); i++) {
        rectangle(image, Point(faces[i].x, faces[i].y), Point(faces[i].x+faces[i].width, faces[i].y+faces[i].height), Scalar( 255, 0, 255 ), 2 , 8, 0);
    }

    imshow("cv2", image);
    waitKey(0);

}

std::vector<Rect> detect(CascadeClassifier face_cascade, CascadeClassifier eye_cascade, Mat image, int pyramidLevels, int selectivity, int smart_eyes) {

    std::clock_t start, end;

    start = std::clock();

    int min_dim = std::min(image.rows, image.cols);
    float scaleFactor = pow((min_dim/(float)24),(1/(float)pyramidLevels));

    int groupThreshold = 0;
    
    std::vector<Rect> faces;
    face_cascade.detectMultiScale(image, faces, scaleFactor);

    if (selectivity > 0) {

        groupThreshold = 3;

        int offset = 5;

        std::vector<Rect> north, east, south, west;

        face_cascade.detectMultiScale(image(cv::Range(0,image.rows-offset), cv::Range(0,image.cols)), north, scaleFactor);
        faces.insert(faces.end(), north.begin(), north.end());

        face_cascade.detectMultiScale(image(cv::Range(0,image.rows), cv::Range(offset,image.cols)), east, scaleFactor);
        faces.insert(faces.end(), east.begin(), east.end());

        face_cascade.detectMultiScale(image(cv::Range(offset,image.rows), cv::Range(0,image.cols)), south, scaleFactor);
        faces.insert(faces.end(), south.begin(), south.end());
        face_cascade.detectMultiScale(image(cv::Range(0,image.rows), cv::Range(0,image.cols-offset)), west, scaleFactor);
        faces.insert(faces.end(), west.begin(), west.end());
        if (selectivity > 1) {

            groupThreshold = 7;

            std::vector<Rect> northeast, southeast, southwest, northwest;

            face_cascade.detectMultiScale(image(cv::Range(0,image.rows-offset), cv::Range(0,image.cols)), northeast, scaleFactor);
            faces.insert(faces.end(), northeast.begin(), northeast.end());
            face_cascade.detectMultiScale(image(cv::Range(0,image.rows), cv::Range(offset,image.cols)), southeast, scaleFactor);
            faces.insert(faces.end(), southeast.begin(), southeast.end());
            face_cascade.detectMultiScale(image(cv::Range(offset,image.rows), cv::Range(0,image.cols)), southwest, scaleFactor);
            faces.insert(faces.end(), southwest.begin(), southwest.end());
            face_cascade.detectMultiScale(image(cv::Range(0,image.rows), cv::Range(0,image.cols-offset)), northwest, scaleFactor);
            faces.insert(faces.end(), northwest.begin(), northwest.end());
        }

    }
	cout<<"Finished selectivity phase"<<endl;
    groupRectangles(faces, groupThreshold, 0.5);

    if (smart_eyes > 0) {
	cout<<"Checking eyes"<<endl;
        std::vector<Rect> temp;
	cout<<"current face vector size = "<<faces.size()<<endl;
        for ( size_t i = 0; i < faces.size(); i++ ) {

            Mat img_face = image(faces[i]);
            
            std::vector<Rect> eyes;

            eye_cascade.detectMultiScale(img_face, eyes);

            if (eyes.size() >= smart_eyes)
                temp.push_back(faces[i]);
        }

        faces = temp;
    } 

    end = std::clock();
    double runtime = (end-start) / (double)CLOCKS_PER_SEC;

    cout << "pyramidLevels=" << pyramidLevels << " selectivity=" << selectivity << " smart_eyes=" << smart_eyes << " runtime=" << runtime << "\n";

}

void* change_pyramid_Num(void* arg){
        int Pyranum = pyramidPara->intPara;
        int newP = 4-(Pyranum - 1);
        cout<<"num of pyramid changes from "<<pyramid<<" to "<<newP<<endl;
        pyramid = newP;
}

void* change_eyes_Num(void* arg){
        int eyesnum = eyesPara->intPara;
        int newE = 5-(eyesnum - 1);
        cout<<"num of eyes changes from "<<eyes<<" to "<<newE<<endl;
        eyes = newE;
}

void* change_select_Num(void* arg){
	int selectnum = selectPara->intPara;
	int news = 5-(selectnum - 1);
	cout<<"num of select chagnes from "<<selectivity<<" to "<<news<<endl;
	selectivity = news;
}

void setupMission(){
	faceMission = new rsdgMission();	
	pyramidPara = new rsdgPara();
	eyesPara = new rsdgPara();
	selectPara = new rsdgPara();

	//register services
	for(int i = 0; i<4; i++){
		string nodename = to_string(20-i*5)+"p";
		faceMission->regService("pyramid", nodename, &change_pyramid_Num, true, make_pair(pyramidPara, i+1));
	}
	for(int j = 0; j<3; j++){
		string nodename = to_string(3-j*1)+"s";
		faceMission->regService("select", nodename, &change_select_Num,true, make_pair(selectPara, j+1));
	}
	for(int k = 0; k<2; k++){
		string nodename = to_string(2-k*2)+"e";
		faceMission->regService("eyes", nodename, &change_eyes_Num, true, make_pair(eyesPara, k+1));
	}
	faceMission -> generateProb(infile);
	faceMission-> setSolver(rsdgMission::GUROBI, rsdgMission::REMOTE);
	faceMission -> setUnitBetweenCheckpoints(UNIT_PER_CHECK);
	faceMission-> setBudget(totSec*1000);
	faceMission->setUnit(totUnit);
	if(offline){
		faceMission -> readCostProfile();
		faceMission -> readMVProfile();
		faceMission -> setOfflineSearch();
	}
	if(TRAINING){
		faceMission->setTraining();
		faceMission->setUnit(1000000);
	}
	if(UPDATE){
		faceMission->setUpdate(true);
	}
	faceMission->addConstraint("pyramid", true);
	faceMission->addConstraint("select", true);
	faceMission->addConstraint("eyes", true);
	cout<<endl<<"RSDG setup finished"<<endl;
}

/** @function main */
int main( int argc, const char** argv ) {
	// process the arguments
	if(argc>=2){
		cout<<"RAPID INVOLVED"<<endl;
		for(int i = 1; i<argc; i++){
			if(!strcmp(argv[i], "-rsdg")) RSDG = true;
			if(!strcmp(argv[i], "-offline")) offline = true;
			if(!strcmp(argv[i], "-b")) totSec = stoi(argv[++i]);
			if(!strcmp(argv[i], "-train")) TRAINING = true;
			if(!strcmp(argv[i], "-update")) UPDATE = true;
			if(!strcmp(argv[i], "-u")) totUnit = stoi(argv[++i]);
		}
	}
    // load cascade XML files
    CascadeClassifier face_cascade, eye_cascade;
    face_cascade.load("req/face_cascade.xml");
    eye_cascade.load("req/eye_cascade.xml");

    std::vector<string> image_list;
    image_list.push_back("pics/monks.jpg");
    image_list.push_back("pics/family.jpg");

    Mat image;
    std::vector<Rect> faces;
	if(RSDG) {
	setupMission();
	}
	
    for (int i = 0; i < image_list.size(); i++) {
	if(RSDG && i%UNIT_PER_CHECK==0){
		faceMission->reconfig();
		faceMission->setLogger();
		if(faceMission->isFailed()) break;
	}

        cout << image_list[i] << '\n';

        // load image
        image = imread(image_list[i]);  
        
        // perform face dectection
        faces = detect(face_cascade, eye_cascade, image, pyramid, selectivity, eyes);
	faces.clear();//comment this out to save memory
	if(RSDG)faceMission->finish_one_unit();
	if(i==image_list.size()-1){
		//rewind to begining
		if(TRAINING || UPDATE)i = -1;
	}
        // display image with faces marked (comment out if doing in bulk)
        //display(image, faces);

    }

    return 0;
    
}
