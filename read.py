with open('log.txt', 'r') as log:
	log_lines = log.read().split('\n')
	with open('log2.txt', 'r') as log2:
		log2_lines = log2.read().split('\n')
		for i in xrange(len(log_lines)):
			file, pyramidLevel, precision, smart_eye, good_faces, total_faces = log_lines[i].split()
			runtime = log2_lines[i].split(' ')[-1]

			print('%s %s %s %s %s %s %s' % (file, pyramidLevel, precision, smart_eye, good_faces, total_faces, runtime))
