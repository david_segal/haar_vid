import haar

def main():

	detector = haar.Detector()

	detector.run_trials(files=['pics/monks.jpg', 'pics/close_far.jpg', 'pics/work.jpg', 'pics/family.jpg', 'pics/children.jpg'], 
		pyramidLevels=[20,15,10,5], precisions=[0,1,2], smart_eyes=[0,2], show_out=True)


if __name__ == '__main__':
	main()
