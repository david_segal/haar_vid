#include "opencv2/objdetect.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include <cmath>

#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;

void display(Mat image, std::vector<Rect> faces) {

    for (size_t i = 0; i < faces.size(); i++) {
        rectangle(image, Point(faces[i].x, faces[i].y), Point(faces[i].x+faces[i].width, faces[i].y+faces[i].height), Scalar( 255, 0, 255 ), 2 , 8, 0);
    }

    imshow("cv2", image);
    waitKey(0);

}

std::vector<Rect> detect(CascadeClassifier face_cascade, CascadeClassifier eye_cascade, Mat image, int pyramidLevels, int selectivity, int smart_eyes) {

    int min_dim = std::min(image.rows, image.cols);
    float scaleFactor = pow((min_dim/(float)24),(1/(float)pyramidLevels));

    int groupThreshold = 0;
    
    std::vector<Rect> faces;
    face_cascade.detectMultiScale(image, faces, scaleFactor);

    if (selectivity > 0) {

        groupThreshold = 3;

        int offset = 5;

        std::vector<Rect> north, east, south, west;

        face_cascade.detectMultiScale(image(cv::Range(0,image.rows-offset), cv::Range(0,image.cols)), north, scaleFactor);
        faces.insert(faces.end(), north.begin(), north.end());

        face_cascade.detectMultiScale(image(cv::Range(0,image.rows), cv::Range(offset,image.cols)), east, scaleFactor);
        faces.insert(faces.end(), east.begin(), east.end());

        face_cascade.detectMultiScale(image(cv::Range(offset,image.rows), cv::Range(0,image.cols)), south, scaleFactor);
        faces.insert(faces.end(), south.begin(), south.end());

        face_cascade.detectMultiScale(image(cv::Range(0,image.rows), cv::Range(0,image.cols-offset)), west, scaleFactor);
        faces.insert(faces.end(), west.begin(), west.end());

        if (selectivity > 1) {

            groupThreshold = 7;

            std::vector<Rect> northeast, southeast, southwest, northwest;

            face_cascade.detectMultiScale(image(cv::Range(0,image.rows-offset), cv::Range(0,image.cols)), northeast, scaleFactor);
            faces.insert(faces.end(), northeast.begin(), northeast.end());

            face_cascade.detectMultiScale(image(cv::Range(0,image.rows), cv::Range(offset,image.cols)), southeast, scaleFactor);
            faces.insert(faces.end(), southeast.begin(), southeast.end());

            face_cascade.detectMultiScale(image(cv::Range(offset,image.rows), cv::Range(0,image.cols)), southwest, scaleFactor);
            faces.insert(faces.end(), southwest.begin(), southwest.end());

            face_cascade.detectMultiScale(image(cv::Range(0,image.rows), cv::Range(0,image.cols-offset)), northwest, scaleFactor);
            faces.insert(faces.end(), northwest.begin(), west.end());

        }

    }

    groupRectangles(faces, groupThreshold, 0.5);

    if (smart_eyes > 0) {

        std::vector<Rect> temp;

        for ( size_t i = 0; i < faces.size(); i++ ) {

            Mat img_face = image(faces[i]);
            
            std::vector<Rect> eyes;

            eye_cascade.detectMultiScale(img_face, eyes);

            if (eyes.size() >= smart_eyes)
                temp.push_back(faces[i]);
        }

        faces = temp;

    } 

}

/** @function main */
int main( int argc, const char** argv ) {

    // load cascade XML files
    CascadeClassifier face_cascade, eye_cascade;
    face_cascade.load("req/face_cascade.xml");
    eye_cascade.load("req/eye_cascade.xml");

    std::vector<string> image_list;
    image_list.push_back("pics/monks.jpg");
    image_list.push_back("pics/family.jpg");

    Mat image;
    int pyramidLevels, selectivity, smart_eyes;
    std::vector<Rect> faces;

    for (int i = 0; i < image_list.size(); i++) {

        cout << image_list[i] << '\n';

        // load image
        image = imread(image_list[i]);  
        
        // this is the part where the RSDG would choose parameters for detect
        pyramidLevels = 20;
        selectivity = 0;
        smart_eyes = 0;

        // perform face dectection
        faces = detect(face_cascade, eye_cascade, image, pyramidLevels, selectivity, smart_eyes);

        // display image with faces marked (comment out if doing in bulk)
        display(image, faces);

    }

    return 0;
    
}