import cv2
import os
import numpy as np
from math import log
from time import time

class Detector:

	'''
	Constructor will verify existance of required cascade files for face detection
	'''
	def __init__(self, face_cascade='req/haarcascade_frontalface_default.xml', eye_cascade='req/haarcascade_eye.xml'):

	    if not os.path.isfile(face_cascade):
	        raise IOError('face_cascade %s cannot be found' % face_cascade)
	    self.face_cascade = cv2.CascadeClassifier(face_cascade)
        
	    if not os.path.isfile(eye_cascade):
	        raise IOError('eye_cascade %s cannot be found' % eye_cascade)
	    self.eye_cascade = cv2.CascadeClassifier(eye_cascade)


	def crop(self, img, dim):

		offset_x = (img.shape[0]-dim[0])/2
		offset_y = (img.shape[1]-dim[1])/2

		return img[offset_x:offset_x+dim[0], offset_y:offset_y+dim[1]]

	'''
	Detect faces in one single image with one set of parameters
	file -- image to be analyzed
	scaleFactor -- (for OpenCV) factor by which sliding window is resized
	precision -- number of times to apply sliding window algorithm
		(0 == only centered, 1 == 0,north,south,east,west, 2 == 1,north_east,south_east,south_west,north_west)
	smart_eyes -- number of eyes to require within a window to consider it to beface
	show_out -- whether or not to show result image
	out_dir -- directory to store result image (do not store if None)
	'''
	def trial(self, file, pyramidLevel, precision, smart_eye, show_out, out_dir):

		# begin timer for trial execution
		st = time()

	    # open file in color (for output) and grayscale (for analysis--cascade file is trained with grayscale features)
	   	img_color = cv2.imread(file, 1) 
	   	#img_color = self.crop(img_color, (853,1280))
	   	img_gray = cv2.cvtColor(img_color, cv2.COLOR_BGR2GRAY)

	   	# pyramidLevel specifies how many levels the image pyramid will have. the more levels the higher the recall, but 
	   	# the longer the runtime. The face cascade is trained to look at 24x24 squares, scaling up by scaleFactor until
	   	# until the window size exceeds that of the minimum dimmension of the image
	   	# scaleFactor is calculated as a function of pyramidLevel because:
	   	# min_dim >= 24*scaleFactor^pyramidLevels --> (min_dim/24)^(1/pyramidLevels) >= scaleFactor
	   	min_dim = min(img_color.shape[:-1])
		scaleFactor = (min_dim/float(24))**(1/float(pyramidLevel))
		print(scaleFactor)

	    # apply one instance on sliding window algorithm 
	   	centered = self.face_cascade.detectMultiScale(img_gray, scaleFactor=scaleFactor)
	    
	    # list of all lists of coordinates of potential faces, will be appended to if precision is higher
	   	all_lists = [centered]

	    # the minimum number of matches required in the proximity of a match to consider it worth keeping
	    # starts as 0 for precision 0, increases if higher precision is used
	   	group_threshold = 0

	    # continue to apply sliding window algorithm for more precise results
	   	if precision > 0:

			#offset = int(scaleFactor**(pyramidLevel-1))
			offset = 5

			# apply the sliding window algorithm four more times, each time at an offset in respective direction
			north = self.face_cascade.detectMultiScale(img_gray[:-offset], scaleFactor=scaleFactor)
			east = self.face_cascade.detectMultiScale(img_gray[:,offset:], scaleFactor=scaleFactor)
			south = self.face_cascade.detectMultiScale(img_gray[offset:], scaleFactor=scaleFactor)
			west = self.face_cascade.detectMultiScale(img_gray[:,:-offset], scaleFactor=scaleFactor)

			# add new lists of coordinates to list of all lists
			all_lists.extend([north, east, south, west])

			# increase number of matches required in proximity of a match to consider it worth keeping, as more matches will now exist
			group_threshold = 3

	        if precision > 1:

				# apply the sliding window an additional four more times, again in each repective direction
				north_east = self.face_cascade.detectMultiScale(img_gray[:-offset, offset:], scaleFactor=scaleFactor)
				south_east = self.face_cascade.detectMultiScale(img_gray[offset:, offset:], scaleFactor=scaleFactor)
				south_west = self.face_cascade.detectMultiScale(img_gray[offset:,:-offset], scaleFactor=scaleFactor)
				north_west = self.face_cascade.detectMultiScale(img_gray[:-offset,:-offset], scaleFactor=scaleFactor)

				# add new lists of coordinates to list of all lists
				all_lists.extend([north_east, south_east, south_west, north_west])

				# increase number of matches required in proximity of a match to consider it worth keeping, as more matches will now exist
				group_threshold = 7
	        

		# flatten list of lists into on singular list of all facial coordinates
		all_faces = [face for l in all_lists for face in l]

		# combine nearby rectangles (nearby defined by eps, 0.5 means atleast 50% overlapping), and new group of rectangles if there are atlesast group_threshold matches in a group
	   	all_faces, _ = cv2.groupRectangles(all_faces, groupThreshold=group_threshold, eps=0.5)
	  
		# if smart_eye is required, run through every face in final face list and only keep that face if it contains at least smart_eye number eyes (usually one or two)
	   	if smart_eye > 0:
	   		temp = []
	   		for (xf,yf,wf,hf) in all_faces:

	   			# run sliding window algorihm using eye cascade in specific region of image where face is identified
	   			eyes_in_face =  self.eye_cascade.detectMultiScale(img_gray[yf:yf+hf, xf:xf+wf])
	   			
	   			for (xe,ye,we,he) in eyes_in_face:
	   				cv2.rectangle(img_color, (xf+xe,yf+ye), (xf+xe+we,yf+ye+he), (0,0,255), 2)

	   			# if region where face is identified contains at least smart_eye number eyes, keep that face
	   			if len(eyes_in_face) >= smart_eye:
	   				temp.append((xf,yf,wf,hf))

			all_faces = temp	

	   	# end time for trial execution
	   	en = time()

	   	 # display output if requested
	   	if show_out:

			# print rectangles around selected faces
			for (xf,yf,wf,hf) in all_faces:
				cv2.rectangle(img_color, (xf,yf), (xf+wf,yf+hf), (0,255,0), 2)

			# show image, and let any key close the window
			cv2.imshow('cv2', img_color)
			cv2.waitKey(0)
			cv2.destroyAllWindows()

		# save output if requested
	   	if out_dir is not None:
	   		file = os.path.basename(file)
	       	ext_ind = file.rfind('.')
	       	if ext_ind == -1 and ext_end != len(file)-1:
	       	    ext_end = len(file)
	       	    suffix = 'png'
	       	else:
	       	    suffix = file[ext_ind+1:]

   		correct_faces = -1
   		
		while correct_faces<0:
			try:
				correct_faces = int(raw_input('correct_faces: '))
			except:
				continue
		
   		msg = 'file=%s\npyramidLevel=%s\nprecision=%s\nsmart_eye=%s\nruntime=%ss\ncorrect_faces=%s\n' % (file, pyramidLevel, precision, smart_eye, en-st, correct_faces)
	
   	   	print(msg)

   	   	with open('log.txt', 'a') as log:
   	   		log.write('%s %s %s %s %s %s\n' % (file,pyramidLevel,precision,smart_eye,correct_faces,len(all_faces)))

	'''
	Run a series of trials as specified by parameters
	'''
	def run_trials(self, files=[''], pyramidLevels=[-1], precisions=[-1], smart_eyes=[-1], show_out=False, out_dir=None):

	    	# validate all parameters and return with error if a valid session cannot be run
	    
		files = filter(lambda x: os.path.isfile(x), files)
		if len(files) < 1:
			raise ValueError('No valid files were supplied')

		scaleFactors = filter(lambda x: int(x) > 0, pyramidLevels)
		if len(scaleFactors) < 1:
		    raise ValueError('No valid scaleFactors were supplied')

		precisions = filter(lambda x: int(x) == 0 or int(x) == 1 or int(x) == 2, precisions)
		if len(precisions) < 1:
		    raise ValueError('No valid precisions were supplied')

		smart_eyes = filter(lambda x: int(x)==x, smart_eyes)
		if len(smart_eyes) < 1:
		    raise ValueError('No valid smart_eyes were supplied')

		if show_out != False and show_out != True:
		    raise ValueError('show_out %s must be False or True' % show_out)

		if out_dir is not None and not os.path.isdir(out_dir):
		    makedirs(out_dir)

		# run every trial specified by parameters, logging information as specified
		for file in files:
		    for pyramidLevel in pyramidLevels:
		        for precision in precisions:
		            for smart_eye in smart_eyes:
		                runtime = self.trial(file, pyramidLevel, precision, smart_eye, show_out, out_dir)
