What Is It:
haar_vid is an application for identifying human faces from frames within a video file. It is highly paramerizable, and was designed to test the trade off space of energy usage vs. quiality of service delivery.

Who Made It:
David Segal
Rutgers University
June 2017

How To Use It:
python haar_vid.py <parameters>

--file					specify video file to run haar_vid on. This is required.\n'
--scaleFactor_f & scaleFactor_e		set the scaleFactor for face, or eyes, respectively. A scaleFactor will reduce the size of a frame to 1/scaleFactor of its original size. It is often beneficial to do so, to detect target objects in an image, but scaling an image too small will make features indistinguishable, causing target objects to be missed. Increasing the scale factor will also likely decrease runtime.\n'
--minNeighbors_f & minNeighbors_e	set the minNeighbor for face, or eyes, respectively. minNeighbors is the required number of target objects found within the vicinty of a target object, in order for it to be marked by the algorithm. A low minNeighbors will cause false positives, and a high minNeighbors could cause misses on actual target object matches.\n'
--out_dir				the path to the directory that output frames should be stored at.\n'
--frame_skip 				the number of frames to skip between each application of the algorithm. Low frame_skip will give a longer runtime, and potential duplicate results, while a high frame_skip can miss key frames in a video.'

What Do I Need to Use It:
-Python 2.7.x				sudo add-apt-repository ppa:fkrull/deadsnakes && sudo apt-get update && sudo apt-get install python2.7

-OpenCV 				sudo apt-get install libopencv-dev python-opencv

-Face Haar Cascade File 		https://raw.githubusercontent.com/opencv/opencv/master/data/haarcascades/haarcascade_frontalcatface_extended.xml

-Eye Haar Cascade File 			https://raw.githubusercontent.com/opencv/opencv/master/data/haarcascades/haarcascade_eye.xml

-A burning desire to identify human faces from within video files

TOOD
Add documentation for smart_eyes
