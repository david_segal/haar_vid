echo 'Use all defaults -- when prompted leave blank and hit enter'

ssh-keygen -t rsa

read -p 'Address: ' ADDR
read -p 'Username: ' UN

echo 'You will be prompted for password twice.'

ssh $UN@$ADDR mkdir -p .ssh

cat /home/$USER/.ssh/id_rsa.pub | ssh $UN@$ADDR 'cat >> .ssh/authorized_keys'


