from sys import argv
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import simps
from numpy import trapz


if len(argv) < 2:
	sys.exit()

log = open(argv[1])
out = open(argv[2], 'w+')


thispow = 0
ct = 0

for line in log:

#	print line

	line = line.rstrip()

	try:
		line = float(line)
		ct+=1
		thispow+=line

	except:
		try:
			
			st, num = line.split(' ')
			
			out.write('{}\t{}\n'.format(num, thispow/ct))
		
		except:
			continue

		thispow = 0
		ct = 0