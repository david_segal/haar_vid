import numpy as np
import cv2
from sys import argv, exit
from os import makedirs
from os.path import isfile, isdir, basename
from time import time
import matplotlib.pyplot as plt
from math import ceil

face_cascade = cv2.CascadeClassifier('req/haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('req/haarcascade_eye.xml')

params = {
	'--file':None, 		
	'--out_dir':'out',
	'--scaleFactor_f': 1.1, 
	'--minNeighbors_f':5,
	'--smart_eyes':0,  
	'--scaleFactor_e':1.1, 
	'--minNeighbors_e':5, 
	'--frame_skip':10,
	'--actual_faces':1,		
}

def param_validate(argv):
	
	if len(argv) % 2 == 0:
		argv.append('')
	
	for param, value in zip(argv[1::2],argv[2::2]):
		if param in params:
			if param == '--file':
				if isfile(value) == False:
					print 'Parameter Validation Failed: file {} does not exist'.format(value)
					return False
			elif param == '--out_dir':
				None #do nothing here, will be set below			
			elif param == '--scaleFactor_f' or param == '--scaleFactor_e':
				try:
					value = float(value)
				except:
					value = -1
				if value <= 1:
					print 'Parameter Validation Failed: scaleFactor must be greater than 1'
					return False
			elif param == '--minNeighbors_f' or param == '--minNeighbors_e':
				try:
					value = int(value)
				except:
					value = -1
				if value < 0:
					print 'Parameter Validation Failed: minNeighbors must be a positive number'
					return False
			elif param == '--smart_eyes':
				try:
					value = int(value)
				except:
					value = -1
				if value < 0 or value > 2:
					print 'Parameter Validation Failed: smart_eyes must be 0, 1, or 2'
					return False	
			elif param == '--frame_skip':
				try:
					value = int(value)
				except:
					value = -1
				if value < 0:
					print 'Parameter Validation Failed: frame_skip must be a positive number'
					return False
			elif param == '--actual_faces':
				try:
					value = int(value)
				except:
					value = -1
				if value < 0:
					print 'Parameter Validation Failed: actual_faces must be 0 or greater'
					return False
			else:
				print 'Parameter Validation Failed: This should not occur, debugging test'
				return False
			
			params[param] = value
		
		elif param == '--help':
			print '--file \t\t specify video file to run haar_vid on. This is required.\n'
			print '--scaleFactor_f & scaleFactor_e \t\t set the scaleFactor for face, or eyes, respectively. A scaleFactor will reduce the size of a frame to 1/scaleFactor of its original size. It is often beneficial to do so, to detect target objects in an image, but scaling an image too small will make features indistinguishable, causing target objects to be missed. Increasing the scale factor will also likely decrease runtime.\n'
			print '--minNeighbors_f & minNeighbors_e \t\t set the minNeighbor for face, or eyes, respectively. minNeighbors is the required number of target objects found within the vicinty of a target object, in order for it to be marked by the algorithm. A low minNeighbors will cause false positives, and a high minNeighbors could cause misses on actual target object matches.\n'
			print '--smart_eyes \t\t the minimum number of eyes that must appear in a face for the face to be marked. Must be 0, 1, or 2.\n'
			print '--out_dir \t\t the path to the directory that output frames should be stored at.\n'
			print '--frame_skip \t\t the number of frames to skip between each application of the algorithm. Low frame_skip will give a longer runtime, and potential duplicate results, while a high frame_skip can miss key frames in a video.'
			return False
		else:
			print 'Parameter Validation Failed: {} is not a valid parameter'.format(param)
			return False

	if params['--file'] == None:
		print 'Parameter Validation Failed: must supply filename'
		return False

	if isdir(params['--out_dir']) == False:
		makedirs(params['--out_dir'])
		
	return True



def main():

	# print configuration
	for param in params:
		print '\t{} = {}'.format(param, params[param])

	# load parameters so there isn't a dictionary access in every loop iteration
	fn = params['--file']
	out_dir = params['--out_dir']	
	sF_f = params['--scaleFactor_f']
	mN_f = params['--minNeighbors_f']
	sM_e = params['--smart_eyes']
	sF_e = params['--scaleFactor_e']
	mN_e = params['--minNeighbors_e']
	fS = params['--frame_skip']
	aF = params['--actual_faces']

	# basename for file output
	fn_base = ''.join(basename(fn).split('.')[:-1])

	# frame count
	fm_count = 0

	# open video stream
	vid_stream  = cv2.VideoCapture(fn)

	# capture start time of run -- everything prior was invariable
	st = time()

	# capture first frame of video
	success, frame = vid_stream.read()

	while success:
	
		save = False

		# convert frame to b&w for better matching results
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

		'''
		pix_count = {}

		l = [0]*256

		for row in  xrange(gray.shape[0]):
			for col in xrange(gray.shape[1]):
				l[gray[(row, col)]]+=1

		plt.plot(l)
		plt.savefig('test.jpg')
		'''

		# use face haar cascade on frame
		faces = face_cascade.detectMultiScale(gray, scaleFactor=sF_e, minNeighbors=mN_f)

		'''
		f = faces.tolist() + faces.tolist()
		print f
		faces, conf = cv2.groupRectangles(f, groupThreshold=1, eps = 1)
		'''
		

		#print conf
		'''
		# if the number of faces is greater than 2 times the expected number of faces, require that each box must have a neighbor to reduce number of faces
		if len(faces) > 2*aF:
			faces, conf = cv2.groupRectangles(list(faces), 1)
		'''
		
		face_count = 0

	
		for (x,y,w,h) in faces:
	
			# a subframe of only a face, to search for eyes on. gray for detecting faces, color for marking eyes
			sub_gray = gray[y:y+h, x:x+w]
			sub_frame = frame[y:y+h, x:x+w]

			# use eye haar cascade on current face
			eyes = eye_cascade.detectMultiScale(sub_gray, scaleFactor=sF_e, minNeighbors=mN_e)
			
			# if enough eyes are found, according to smart_eyes, mark them and mark the image worth saving
			if len(eyes) >= sM_e:

				# draw rectangle around faces
				cv2.rectangle(frame, (x,y), (x+w, y+h), (255,0,0), 2)

				for (ex, ey, ew, eh) in eyes:

					# draw rectangle around all found eyes
					cv2.rectangle(sub_frame, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 2)

				face_count+=1
		
		'''
		# save image if it satisfies rules
		if face_count > 0:
			print 'saved',
			cv2.imwrite('{}/{}_{}.jpg'.format(out_dir,fn_base,fm_count), frame)
		
		# skip frame_skip number of frames
		for _ in range(fS):
			success, frame = vid_stream.read()
			if success == False:
				break
			fm_count+=1
		'''

		break

	dur = time() - st

	vid_stream.release()

	print '\n{} faces found, and {} actual faces ({})'.format(face_count, aF, face_count-aF)
	cv2.imshow('cv2', frame)
	cv2.waitKey(0)
	cv2.destroyAllWindows()

	'''
	false_positives = None
	while false_positives == None:
		false_positives = int(input('\nfalse positives: '))


	with open('log.txt', 'a') as log:
		log.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(fn, sF_f, mN_f, sM_e, sF_e, mN_e, aF, face_count-false_positives, false_positives, dur, -1, -1))
	'''
	
	return dur

def trials():

	files = [('vid/family.jpg', 5), ('vid/work.jpg', 5), ('vid/monks.jpg', 37), ('vid/children.jpg', 3), ('vid/close_far.jpg', 2)]
	scaleFactor_f = [1.1]
	minNeighbors_f = [5]

	smart_eyes = [0]
	scaleFactor_e = [1.1]
	minNeighbors_e = [5]

	frame_skip = [10]

	iterations = 1

	for f in files:
		for sF_f in scaleFactor_f:
			for mN_f in minNeighbors_f:
				for sM_e in smart_eyes:
					for sF_e in scaleFactor_e:
						for mN_e in minNeighbors_e:
							for fS in frame_skip:
								param_validate([None,'--file', f[0], '--scaleFactor_f', sF_f, '--minNeighbors_f', mN_f, '--smart_eyes', sM_e, '--scaleFactor_e', sF_e, '--minNeighbors_e', mN_e, '--frame_skip', fS, '--actual_faces', f[1]])
								run = 0
								for _ in range(iterations):
									run += main()
								'''
								print '\n\n{}\n\n'.format(run/iterations)
								with open('log.txt', 'a') as log:
									log.write('rt {}\n'.format(run/iterations))
								'''

if __name__ == "__main__":
	trials()
	'''if param_validate(argv) == False:
		exit()
	main()'''
