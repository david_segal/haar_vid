from sys import argv, exit
from time import time
import numpy as np
import matplotlib.pyplot as plt

if len(argv) < 2:
	exit()

file = open(argv[1])
pr = []
re = []
sF_f = []

p = True;

for line in file:

	if line == 'br\n':

		pr.sort(key=lambda x: x[3])
		re.sort(key=lambda x: x[3])
		sF_f.sort(key=lambda x: x[1])

		if p == True:
			plt.plot([x[3] for x in pr], [x[4] for x in pr], color='blue', label='precision')
			plt.plot([x[3] for x in re], [x[4] for x in re],  color='red', label='recall')
			plt.plot([x[3] for x in re], [x[0] for x in re],  color='green', label='scaleFactor_f')
			#plt.plot([x[3] for x in re], [x[1] for x in re],  color='pink', label='minNeighbors_f')

			p = False
		else:
			plt.plot([x[3] for x in pr], [x[4] for x in pr], color='blue')
			plt.plot([x[3] for x in re], [x[4] for x in re], color='red')
			plt.plot([x[3] for x in re], [x[0] for x in re],  color='green')
			#plt.plot([x[3] for x in re], [x[1] for x in re],  color='pink')

		pr = []
		re = []

		continue

	#print line
	s = line.split("\t")
	try:
		scaleFactor_f, minNeighbors_f, smart_eyes, scaleFactor_e, minNeighbors_e, actual_faces, actual_faces_found,	false_positives, avg_runtime_s,	avg_power_consumption_w, avg_energy_consumption_j, precision, recall = [float(x) for x in s[1:len(s)-3]]
	except:
		print s
		continue

	if smart_eyes == 0 and minNeighbors_f == 0:
		pr.append((scaleFactor_f, minNeighbors_f, smart_eyes, avg_energy_consumption_j, precision))
		re.append((scaleFactor_f, minNeighbors_f, smart_eyes, avg_energy_consumption_j, recall))

#plt.ylim([-0.1,1.1])
plt.legend()
plt.show()
plt.clf()