# params
con='ubnt@astro.cs.rutgers.edu'
port='3'
out_dir='out'
out_log='log.txt'
delim='_'

file='con/family.jpg'
scaleFactor_f=(1.25 1.5 1.75)
minNeighbors_f=(3 5 7)
scaleFactor_e=(1.1)
minNeighbors_e=(5)
smart_eyes=(0)
frame_skip=(10)

rm -r $out_dir
mkdir $out_dir
touch $out_dir/$out_log

# launch logging in new process (no need to pass PW )
bash log.sh $con $port $out_dir/$out_log &

# sleep to allow logging to initiate
sleep 15

# print qs to log indicate the beginning of trials
echo 'hv' | tee -a $out_dir/$out_log

# iterate through options, storing the start and finish time of each in $OUTRUN
for sF_f in ${scaleFactor_f[@]}
do
#        for mN_f in ${minNeighbors_f[@]}
#        do
#		for sF_e in ${scaleFactor_e[@]}
#		do
#			for mN_e in ${minNeighbors_e[a]}
#			do
#				for sE in ${smart_eyes[@]}
#				do
#					for fS in ${frame_skip[@]}
#					do
						
						python haar_vid.py --file $file --scaleFactor_f $sF_f --minNeighbors_f 5 --scaleFactor_e 1.1 --minNeighbors_e 5 --smart_eyes 0 --frame_skip 10 --out_dir $out_dir/$sF_f$delim$mN_f$delim$sF_e$delim$mN_e$delim$sE$delim$fS
						# | tee -a $out_dir/$out_log
						sleep 5
#					
#					done
#				done
#			done
#		done
 #       done
done

# kill logging process
kill -9 $!

# send logging and running information toilab
scp -r $out_dir 'dis43@flyweight.cs.rutgers.edu:~/Desktop'

