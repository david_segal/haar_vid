import os
import time 
import cv2
import matplotlib.pyplot as plt
import numpy as np
import paramiko
from multiprocessing import Process, Pool, cpu_count, Queue as Proc_Queue
from time import sleep
from math import log
from itertools import chain

def merge_worker(inner, outer, crit, wiggle):

        if crit == -1 and inner is not outer:
            print('failed retain operation')
            return None

        '''
        Note:

        If crit==-1, 
            this is a special instance of a merge, known as a retain. 
            In this case, inner and outer are the same list, and return 
            a list of all rects that are not entirely encompassed by another rect
        otherwise
            this is a normal merge.
            in this case, inner and outer are two different lists, and return 
            a list of all rects from outer that contain crit items from inner in them

        There are not two functions for this becuase it is essentially the same function
        '''

        # dictionary of all rects in outer list
        count_dict = dict.fromkeys([tuple(x) for x in outer], 0)


        for (x1,y1,w1,h1) in inner:
            for (x2,y2,w2,h2) in outer:

                x = x2<=(x1+wiggle)
                y = y2<=(y1+wiggle)
                xw = x2+w2>=(x1+w1)-wiggle
                yh = y2+h2>=((y1+h1)-wiggle)

                # if inner[(x1,y1,w1,h1)] is (somewhat) contained in outer[(x2,y2,w2,h2)],
                if (x and xw) or (y and yh):

                    # if crit==-1, decrement at (x1,y1,w1,h1) so it won't be reatined
                    if crit == -1:
                        '''
                        if count_dict[(x2,y2,w2,h2)] < -1:
                            continue
                        '''
                        count_dict[(x1,y1,w1,h1)]-= 1
                        '''
                        if count_dict[(x1,y1,w1,h1)] < -1:
                            break
                        '''
                    # else, increment at (x2,y2,w2,h2) so it will be used in output list 
                    else:
                        count_dict[(x2,y2,w2,h2)]+=1

        return [x for x in count_dict if count_dict[x] >= crit]

def merge(inner, outer, crit, wiggle):

    cpu_ct = cpu_count()

    # through testing it was found that multiprocessing is only beneficial on machines with >4 cores
    if cpu_ct > 4:

        pool = Pool(cpu_ct)

        # split inner into len(inner)/cpu_ct chunks of roughly even size
        inner_chunks = [inner[i::cpu_ct] for i in xrange(cpu_ct)]

        # execute merge_worker on one chunk on each core
        worker_results = [pool.apply(merge_worker, args=(inner_chunk, outer, crit, wiggle)) for inner_chunk in inner_chunks]

        # chain worker_results together to get a single flattened list
        result = chain.from_iterable(worker_results)
    else:

        # execute merge_worker on entire inner list, multiprocessing won't be beneficial
        result = merge_worker(inner, outer, crit, wiggle)

    print(len(result))
    return set(result)


# a wrapper class for gathering data from opencv cascade classification trials
class Detector:

    # initialize detector by validating parameters
    def __init__(self, face_cascade='req/haarcascade_frontalface_default.xml', eye_cascade='req/haarcascade_eye.xml'):

        if not os.path.isfile(face_cascade):
            raise IOError('face_cascade %s cannot be found' % face_cascade)
        self.face_cascade = cv2.CascadeClassifier(face_cascade)
        
        if not os.path.isfile(eye_cascade):
            raise IOError('eye_cascade %s cannot be found' % eye_cascade)
        self.eye_cascade = cv2.CascadeClassifier(eye_cascade)


    # run on single trial of a face detection
    def trial(self, file, scaleFactor, precision, smart_eye, show_out, out_dir):

        st = time.time()

        # open file in color, for output, and grayscale, for analysis (classifier is trained with grayscale features)
        img_color = cv2.imread(file, 1) 
        img_gray = cv2.cvtColor(img_color, cv2.COLOR_BGR2GRAY)

        # apply sliding window to original image without any cropping
        centered = self.face_cascade.detectMultiScale(img_gray, scaleFactor=scaleFactor)
        all_lists = [centered]
        gt = 0

        # if a higher precision is requested, apply sliding window to image shifted in multiple directions by some offset
        # chosen experimentally. The goal is to identify boxes that were missed by the original sliding window for centered,
        # and also to be able to say with more confidence if a chunk of an image has a face in it or not
        if precision > 0:

            # the image pyramid starts at level 0, with img_color at its inital size. At each level, img_color is halved in
            # size, until the smaller dimmension in the image can no longer fit the sliding window. There will be 
            # int(log(min_dim/24, scaleFactor)) levels of the pyramid because this solves for n in min_dim = 24*scaleFactor^n. The i'th level 
            # of the pyramid jumps scaleFactor^i pixels per window. Thus, we take offset to be 2**(levels_of_pyramid-1) because this is
            # the mean number of pixels jumped over all levels of the image pyramid. Note classifier window is of size 24x24

            # window jumps by n pixels where n the scale by which the original window (24x24) is multiplied
            min_dim = min(img_color.shape[:-1])
            levels_of_pyramid = int(log(min_dim/24, scaleFactor))
            offset = int(scaleFactor**(levels_of_pyramid-1))

            gt = 3
           

            # precision of 1, apply the sliding window to four more versions of the image
            north = self.face_cascade.detectMultiScale(img_gray[:-offset], scaleFactor=scaleFactor)
            east = self.face_cascade.detectMultiScale(img_gray[:,offset:], scaleFactor=scaleFactor)
            south = self.face_cascade.detectMultiScale(img_gray[offset:], scaleFactor=scaleFactor)
            west = self.face_cascade.detectMultiScale(img_gray[:,:-offset], scaleFactor=scaleFactor)
            
            all_lists.extend([north, east, south, west])

            if precision > 1:
    
                gt = 7

                # precision of 2, apply the sliding window to four more including the previous four versions of the iamge
                north_east = self.face_cascade.detectMultiScale(img_gray[:-offset, offset:], scaleFactor=scaleFactor)
                south_east = self.face_cascade.detectMultiScale(img_gray[offset:, offset:], scaleFactor=scaleFactor)
                south_west = self.face_cascade.detectMultiScale(img_gray[offset:,:-offset], scaleFactor=scaleFactor)
                north_west = self.face_cascade.detectMultiScale(img_gray[:-offset,:-offset], scaleFactor=scaleFactor)

                all_lists.extend([north_east, south_east, south_west, north_west])
            

        all_faces = []

        for lst in all_lists:
            if len(lst) > 0:
                all_faces.extend(lst.tolist())

        all_faces, trash = cv2.groupRectangles(all_faces, groupThreshold=gt, eps=0.5)

        print all_faces

        for (xf,yf,wf,hf) in all_faces:
            cv2.rectangle(img_color, (xf,yf), (xf+wf,yf+hf), (0,255,0), 2)
       
        en = time.time()

        # display output if requested
        if show_out:
            cv2.imshow('cv2', img_color)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        # save output if requested
        if out_dir is not None:
            file = os.path.basename(file)
            ext_ind = file.rfind('.')
            if ext_ind == -1 and ext_end != len(file)-1:
                ext_end = len(file)
                suffix = 'png'
            else:
                suffix = file[ext_ind+1:]
            cv2.imwrite('%s/%s_%s_%s_%s.%s' % (out_dir, file[0:ext_ind], scaleFactor, preicision, smart_eye, suffix), img_color)

        return en-st


# a class for automation of common tasks
class Automator:

    # print a stylized box of output values split on newline
    def print_box(self, message):

        columns = int(os.popen('stty size', 'r').read().split()[1])
        columns = int(columns)
        lines = message.strip().split('\t')
        maxlen = len(max(lines, key=len))
        offset = ' '*((columns/2)-maxlen)

        print(offset+'~'*(maxlen+8))
        for line in lines:
            print(offset+'#  %s%s  #' % (line, ' '*(maxlen-(len(line)-1))))
        print(offset+'~'*(maxlen+8)+'\n')

    def run_ssh_session(self, ssh_login, queue):
        
        # forces automatic closing of ssh_session when process is terminated   
        with paramiko.SSHClient() as ssh_session:
            ssh_session.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh_session.connect(ssh_login[0], username=ssh_login[1], password=ssh_login[2])
            while True:
                stdin, stdout, stderr = ssh_session.exec_command('cat ../../../proc/power/active_pwr%s' % ssh_login[3])
                queue.put(stdout.readline())
                sleep(0.25)

    # run all trials specified user
    def run_trials(self, detector=None, out_dir=None, files=None, scaleFactors=[1.01], precisions=[0], smart_eyes=[0], show_out=False, log_file=None, ssh_login=None):

        # validate all parameters and return with error if a valid session cannot be run
        
        if detector is None:
            return False

        if out_dir is not None and not os.path.isdir(out_dir):
            makedirs(out_dir)

        files = filter(lambda x: os.path.isfile(x), files)
        if len(files) < 1:
            print('No valid files were supplied')
            return False

        scaleFactors = filter(lambda x: x > float(1.0), scaleFactors)
        if len(scaleFactors) < 1:
            print('No valid scaleFactors were supplied')
            return False

        precisions = filter(lambda x: int(x) == 0 or int(x) == 1 or int(x) == 2, precisions)
        if len(precisions) < 1:
            print('No valid precisions were supplied')
            return False

        smart_eyes = filter(lambda x: int(x)==x, smart_eyes)
        if len(smart_eyes) < 1:
            print('No valid smart_eyes were supplied')
            return False

        if show_out != False and show_out != True:
            print('show_out %s must be False or True' % show_out)
            return False

        log = None
        if log_file is not None:
            mode = 'w'
            if os.path.isfile(log_file):
                mode = 'a'
            log = open(log_file, mode)

        ssh_proc = None
        if ssh_login is not None and log_file is not None and len(ssh_login) == 4:
            proc_queue = Proc_Queue()
            ssh_proc = Process(target=self.run_ssh_session, args=(ssh_login,proc_queue,))
            ssh_proc.start()

        # run every trial specified by parameters, logging information as specified
        out_str = ''
        for file in files:
            for scaleFactor in scaleFactors:
                for precision in precisions:
                    for smart_eye in smart_eyes:
                        
                        runtime = detector.trial(file, scaleFactor, precision, smart_eye, show_out, out_dir)
                        
                        out_line = 'file=%s\tscaleFactor=%s\tprecision=%s\tsmart_eye=%s\truntime=%ss\n' % (file, scaleFactor, precision, smart_eye, runtime)
                        
                        # if an ssh_proc is running, dump contents of synchronized queue (power readings) from previous trial while clearing proc_queue
                        if ssh_proc is not None:
                            while not proc_queue.empty():
                                log.write('%s' % proc_queue.get())

                        # if a log_file was specified, dump trial information to it
                        if log_file is not None:
                            log.write(out_line)

                        self.print_box(out_line)
                        out_str += out_line

                    out_str += '\n'
    
        # if an ssh_proc was initialized, terminate it, process will try its best to gracefully end ssh_session
        if ssh_proc is not None:
            ssh_proc.terminate()

        if log is not None:
            log.close()

        return True

    # plot a file generated by Automator.run_trials() with runtime as the dependant variable
    def plot_time(self, log_file, ind_var):

        if ind_var != 'scaleFactor' and ind_var != 'minNeighbor' and ind_var != 'smart_eye':
            return False

        x = []
        y = []

        with open(log_file, 'r') as log:
            entries = (log.read().strip()).split('\n\n')
            for entry in entries:
                file, scaleFactor, minNeighbor, smart_eye, runtime = [z.split(' = ')[1] for z in entry.split('\n')]
                if ind_var == 'scaleFactor':
                    x.append(scaleFactor)
                elif ind_var == 'minNeighbor':
                    x.append(minNeighbor)
                elif ind_var == 'smart_eye':
                    x.append(smart_eye)
                y.append(runtime)

        plt.plot(x, y)
        plt.show()
